package io.determan.jschema.validator;

import io.determan.jschema.validator.constrains.ExclusiveMaximum;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ExclusiveMaximumValidatorForInteger implements ConstraintValidator<ExclusiveMaximum, Integer> {

    protected long maxValue;

    @Override
    public void initialize(ExclusiveMaximum max) {
        maxValue = max.value();
    }

    @Override
    public boolean isValid(Integer value, ConstraintValidatorContext context) {
        // null values are valid
        if ( value == null ) {
            return true;
        }

        return value < maxValue;
    }
}
