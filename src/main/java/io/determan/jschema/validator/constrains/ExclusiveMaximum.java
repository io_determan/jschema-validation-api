package io.determan.jschema.validator.constrains;


import io.determan.jschema.validator.ExclusiveMaximumValidatorForInteger;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
@Retention(RUNTIME)
@Repeatable(ExclusiveMaximum.List.class)
@Documented
@Constraint(validatedBy = {ExclusiveMaximumValidatorForInteger.class})
public @interface ExclusiveMaximum {

    String message() default "{io.determan.validation.constraints.ExclusiveMaximum.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * @return value the element must be lower to
     */
    long value();

    /**
     * Defines several {@link ExclusiveMaximum} annotations on the same element.
     *
     * @see ExclusiveMaximum
     */
    @Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
    @Retention(RUNTIME)
    @Documented
    @interface List {
        ExclusiveMaximum[] value();
    }
}
