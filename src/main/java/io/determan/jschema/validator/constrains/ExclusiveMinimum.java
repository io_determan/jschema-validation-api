package io.determan.jschema.validator.constrains;

import io.determan.jschema.validator.ExclusiveMinimumValidatorForInteger;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
@Retention(RUNTIME)
@Repeatable(ExclusiveMinimum.List.class)
@Documented
@Constraint(validatedBy = {ExclusiveMinimumValidatorForInteger.class})
public @interface ExclusiveMinimum {
    String message() default "{io.determan.validation.constraints.ExclusiveMinimum.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * @return value the element must be higher to
     */
    long value();

    /**
     * Defines several {@link ExclusiveMinimum} annotations on the same element.
     *
     * @see ExclusiveMinimum
     */
    @Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
    @Retention(RUNTIME)
    @Documented
    @interface List {
        ExclusiveMinimum[] value();
    }
}
