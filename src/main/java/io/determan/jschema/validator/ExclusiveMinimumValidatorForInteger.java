package io.determan.jschema.validator;

import io.determan.jschema.validator.constrains.ExclusiveMinimum;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ExclusiveMinimumValidatorForInteger implements ConstraintValidator<ExclusiveMinimum, Integer> {

    protected long minValue;

    @Override
    public void initialize(ExclusiveMinimum min) {
        minValue = min.value();
    }

    @Override
    public boolean isValid(Integer value, ConstraintValidatorContext context) {
        // null values are valid
        if ( value == null ) {
            return true;
        }

        return value > minValue;
    }
}
